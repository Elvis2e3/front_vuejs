# front_2e3

## Project setup
```
yarn install
```
### Config API
File `src/services/config.js`
```javascript
const configService = {
    apiUrl: 'http://localhost:3000'
}

export default configService;
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
