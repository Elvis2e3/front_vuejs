import back2e3 from "./back_2e3";

const apiBack2e3 = {}
apiBack2e3.users = function (id){
    let url = '/users/'
    if (id){
        url = `${url}${id}`;
    }
    return back2e3.get( url, {
        params: { },
    }).then(res => res.data).catch(e => e);
}
apiBack2e3.delete_user = function (id){
    let url = '/users/'
    if (id){
        url = `${url}${id}`;
        return back2e3.delete( url, {
            params: { },
        }).then(res => res.data).catch(e => e);
    }
}

apiBack2e3.products = function (id){
    let url = '/products/'
    if (id){
        url = `${url}${id}`;
    }
    return back2e3.get(url, {
        params: { },
    }).then(res => res.data).catch(e => e);
}
apiBack2e3.delete_product = function (id){
    let url = '/products/'
    if (id){
        url = `${url}${id}`;
        return back2e3.delete( url, {
            params: { },
        }).then(res => res.data).catch(e => e);
    }
}

apiBack2e3.orders = function (id){
    let url = '/orders/'
    if (id){
        url = `${url}${id}`;
    }
    return back2e3.get(url, {
        params: { },
    }).then(res => res.data).catch(e => e);
}

apiBack2e3.delete_order = async function (id){
    let url = '/orders/'
    if (id){
        url = `${url}${id}`;
        const order = await this.orders(id)
        order.items.forEach(async (item)=>{
            await this.delete_orderItems(item.id)
        })
        return back2e3.delete( url, {
            params: { },
        }).then(res => res.data).catch(e => e);
    }
}

apiBack2e3.create_order = async function (order_data){
    let url = '/orders/'
    if (order_data){
        // const order = await this.orders(id)
        // order.items.forEach(async (item)=>{
        //     await this.delete_orderItems(item.id)
        // })
        const order =  await back2e3.post( url, {
            user: order_data.user,
            status: true
        });
        return back2e3.post( '/order-items/', {
            order: order.data,
            product: order_data.product,
            quantity: order_data.quantity,
            status: true
        }).then(res => res.data).catch(e => e);
    }
}

apiBack2e3.orderItems = function (id){
    let url = '/order-items/'
    if (id){
        url = `${url}${id}`;
    }
    return back2e3.get(url, {
        params: { },
    }).then(res => res.data).catch(e => e);
}

apiBack2e3.delete_orderItems = function (id){
    let url = '/order-items/'
    if (id){
        url = `${url}${id}`;
        return back2e3.delete( url, {
            params: { },
        }).then(res => res.data).catch(e => e);
    }
}

export default apiBack2e3
