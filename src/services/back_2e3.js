import axios from 'axios';
import configService from "./config";

const back2e3 = axios.create({
    baseURL: configService.apiUrl,
    headers: {
        'Content-Type': 'application/json'
    }

})

export default back2e3
