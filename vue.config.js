const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  pages: {
    'index': {
      entry: './src/pages/Home/main.js',
      title: 'Home',
      chunks: [ 'chunk-vendors', 'chunk-common', 'index' ]
    },
    'users': {
      entry: './src/pages/Users/user.main.js',
      title: 'Usuarios',
      chunks: [ 'chunk-vendors', 'chunk-common', 'users' ]
    },
    'products': {
      entry: './src/pages/Products/product.main.js',
      title: 'Productos',
      chunks: [ 'chunk-vendors', 'chunk-common', 'products' ]
    },
    'orders': {
      entry: './src/pages/Orders/order.main.js',
      title: 'Pedidos',
      chunks: [ 'chunk-vendors', 'chunk-common', 'orders' ]
    }
  }
})
